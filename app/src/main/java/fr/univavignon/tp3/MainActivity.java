package fr.univavignon.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity
{
    //SportDbHelper dbHelper = new SportDbHelper(this);
    SportDbHelper dbHelper;
    //SQLiteDatabase db = dbHelper.getWritableDatabase();
    private ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHelper = new SportDbHelper(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivityForResult(intent,1); //le requestCode (ici 1) permet d'identifier l'activité ouverte, le traitement qu'on en fait, afin de pouvoir récupérer les données de l'activité correspondant à ce requestCode, et pas à une autre activité.

            }
        });


        listview = findViewById(R.id.lstView);
        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
            android.R.layout.simple_list_item_2,
            dbHelper.fetchAllTeams(),
            new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.
            COLUMN_LEAGUE_NAME },
            new int [] { android.R.id.text1, android.R.id.text2});

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                // Do something in response to the click
                Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                Cursor teamChosen = (Cursor) adapter.getItem(position);
                Team team = dbHelper.cursorToTeam(teamChosen);
                intent.putExtra(Team.TAG,team);
                startActivityForResult(intent,2);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 1 && resultCode == NewTeamActivity.RESULT_OK)
        {
            Team team = data.getParcelableExtra(Team.TAG);
            //dbHelper.addTeam(team,db);
            dbHelper.addTeam(team);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        //actualisation de la listview

        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllTeams(),
                new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.
                        COLUMN_LEAGUE_NAME },
                new int [] { android.R.id.text1, android.R.id.text2});

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                // Do something in response to the click
                Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                Cursor teamChosen = (Cursor) adapter.getItem(position);
                Team team = dbHelper.cursorToTeam(teamChosen);
                intent.putExtra(Team.TAG,team);
                startActivityForResult(intent,2);
            }
        });
    }
}
