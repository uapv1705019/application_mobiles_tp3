package fr.univavignon.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id=R
 * Responses must be provided in JSON.
 *
 */

public class JSONResponseHandlerLastEvents
{

    private static final String TAG = JSONResponseHandlerLastEvents.class.getSimpleName();

    private Team team;
    private Match match = new Match();


    public JSONResponseHandlerLastEvents(Team team)
    {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return Something with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException
    {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try
        {
            readLastEvents(reader);
        }
        finally
        {
            reader.close();
            team.setLastEvent(match);// à la fin des manipulations , association du Match créé au dessus et remplit en dessous à team.setLastEvent
        }
    }

    public void readLastEvents(JsonReader reader) throws IOException
    {
        reader.beginObject();
        while (reader.hasNext())
        {
            String name = reader.nextName();
            if (name.equals("results"))
            {
                readArrayLastEvents(reader);
            }
            else
            {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayLastEvents(JsonReader reader) throws IOException //ne sachant pas encore quelles informations me sont nécessaires, je récupère tout
    {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array (the last event is the first in the JSON file)
        while (reader.hasNext() )
        {
            reader.beginObject();
            while (reader.hasNext())
            {
                String name = reader.nextName();
                if (nb==0)
                {
                    if (name.equals("strEvent"))
                    {
                        match.setLabel(reader.nextString());
                    }
                    else if (name.equals("strHomeTeam"))
                    {
                        match.setHomeTeam(reader.nextString());
                    }
                    else if (name.equals("strAwayTeam"))
                    {
                        match.setAwayTeam(reader.nextString());
                    }
                    else if (name.equals("intHomeScore"))
                    {
                        match.setHomeScore(reader.nextInt());
                    }
                    else if (name.equals("intAwayScore"))
                    {
                        match.setAwayScore(reader.nextInt());
                    }
                    else
                    {
                        reader.skipValue();
                    }
                }
                else
                {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
