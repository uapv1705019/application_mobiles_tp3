package fr.univavignon.tp3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Barrier;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;


import fr.univavignon.tp3.Match;
import fr.univavignon.tp3.Team;
import fr.univavignon.tp3.WebServiceUrl;

public class TeamActivity extends AppCompatActivity
{

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;
    private Barrier barrier2;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        barrier2 = (Barrier) findViewById(R.id.barrier2);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
            // TODO : download latest information from online server
                DownloadDataFromServerTask download = new DownloadDataFromServerTask();
                URL url1 = null, url2 = null, url3 = null;
                try
                {
                    url1 = WebServiceUrl.buildSearchTeam(team.getName()); //création des URLs qui seront envoyés à l'AsyncTask
                    url2 = WebServiceUrl.buildGetRanking(team.getIdLeague());
                    url3 = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }
                download.execute(url1,url2,url3); //envoi des URLs à l'AsyncTask
                updateView();

                //TODO : a supprimer quand "mettre à jour" sera fonctionnel
                Snackbar.make(v, "Désolé, cette fonctionnalité n'est pas disponible", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }
        });

    }

    @Override
    public void onBackPressed()
    {
        //TODO : prepare result for the main activity
        super.onBackPressed();
    }

    private void updateView()
    {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

	//TODO : update imageBadge from online server : use team.getTeamBadge
    }

}
