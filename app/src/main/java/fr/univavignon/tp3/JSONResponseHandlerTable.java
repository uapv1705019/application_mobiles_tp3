package fr.univavignon.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=R&s=S
 * Responses must be provided in JSON.
 *
 */

public class JSONResponseHandlerTable
{

    private static final String TAG = JSONResponseHandlerLastEvents.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerTable(Team team)
    {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return Something with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException
    {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try
        {
            readTables(reader);
        }
        finally
        {
            reader.close();
        }
    }

    public void readTables(JsonReader reader) throws IOException
    {
        reader.beginObject();
        while (reader.hasNext())
        {
            String name = reader.nextName();
            if (name.equals("table"))
            {
                readArrayTables(reader);
            }
            else
            {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTables(JsonReader reader) throws IOException //ne sachant pas encore quelles informations me sont nécessaires, je récupère tout
    {
        reader.beginArray();
        int ranking = 1; //utilisé pour calculer le rang de la Team (dans le JSON, la première Team inscrite est classée 1èer, la seconde 2nde...)
        long IdTeam = 0; //utilisé pour conserver une trace du teamId lu dans le JSON, afin de la comparer à la Team recherchée
        while (reader.hasNext() )
        {
            reader.beginObject();
            while (reader.hasNext())
            {
                String name = reader.nextName();
                if (name.equals("teamid"))
                {
                    IdTeam = reader.nextLong();
                }
                else if(name.equals("total"))
                {
                    if (IdTeam == team.getIdTeam())
                    {
                        team.setTotalPoints(reader.nextInt());
                        team.setRanking(ranking);
                    }
                }
                else
                {
                    reader.skipValue();
                }
            }
            reader.endObject();
            ranking++;
        }
        reader.endArray();
    }

}
